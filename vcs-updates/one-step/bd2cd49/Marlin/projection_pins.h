#define PINS_H

#define X_MS1_PIN -1
#define X_MS2_PIN -1
#define Y_MS1_PIN -1
#define Y_MS2_PIN -1
#define Z_MS1_PIN -1
#define Z_MS2_PIN -1
#define E0_MS1_PIN -1
#define E0_MS2_PIN -1
#define E1_MS1_PIN -1
#define E1_MS2_PIN -1
#define DIGIPOTSS_PIN -1

#if MOTHERBOARD_EQ_99
#define	KNOWN_BOARD 1

#define X_STEP_PIN          2
#define X_DIR_PIN           3
#define X_ENABLE_PIN        -1
#define X_STOP_PIN          16

#define Y_STEP_PIN          5
#define Y_DIR_PIN           6
#define Y_ENABLE_PIN       -1
#define Y_STOP_PIN          67

#define Z_STEP_PIN          62
#define Z_DIR_PIN           63
#define Z_ENABLE_PIN       -1
#define Z_STOP_PIN          59

#define E0_STEP_PIN         65
#define E0_DIR_PIN          66
#define E0_ENABLE_PIN      -1

#define SDPOWER            -1
#define SDSS               53
#define LED_PIN            -1
#define FAN_PIN            -1
#define PS_ON_PIN           9
#define KILL_PIN           -1

#define HEATER_0_PIN        13
#define HEATER_1_PIN       -1
#define HEATER_2_PIN       -1
#define TEMP_0_PIN          6   // MUST USE ANALOG INPUT NUMBERING NOT DIGITAL OUTPUT NUMBERING!!!!!!!!!
#define TEMP_1_PIN         -1   // MUST USE ANALOG INPUT NUMBERING NOT DIGITAL OUTPUT NUMBERING!!!!!!!!!
#define TEMP_2_PIN         -1   // MUST USE ANALOG INPUT NUMBERING NOT DIGITAL OUTPUT NUMBERING!!!!!!!!!
#define HEATER_BED_PIN      4
#define TEMP_BED_PIN       10

#endif //MOTHERBOARD_EQ_99

/****************************************************************************************
* Gen7 v1.1, v1.2, v1.3 pin assignment
*
****************************************************************************************/


#if MOTHERBOARD_EQ_12
#define MOTHERBOARD 11
#define GEN7_VERSION 13 // v1.3
#endif //MOTHERBOARD_EQ_12

#if MOTHERBOARD_EQ_11
#define KNOWN_BOARD

#if (!defined(__AVR_ATmega644P__) && (!defined(__AVR_ATmega644__) && !defined(__AVR_ATmega1284P__)))
#error Oops! Make sure you have 'Gen7' selected from the 'Tools -> Boards' menu.

#endif //(!defined(__AVR_ATmega644P__) && (!defined(__AVR_ATmega644__) && !defined(__AVR_ATmega1284P__)))

#if !defined(GEN7_VERSION)
#define GEN7_VERSION 12 // v1.x
#endif //!defined(GEN7_VERSION)

//x axis pins
#define X_STEP_PIN 19
#define X_DIR_PIN 18
#define X_ENABLE_PIN 24
#define X_STOP_PIN 7

//y axis pins
#define Y_STEP_PIN 23
#define Y_DIR_PIN 22
#define Y_ENABLE_PIN 24
#define Y_STOP_PIN 5

//z axis pins
#define Z_STEP_PIN 26
#define Z_DIR_PIN 25
#define Z_ENABLE_PIN 24
#define Z_MIN_PIN 1
#define Z_MAX_PIN 0

//extruder pins
#define E0_STEP_PIN 28
#define E0_DIR_PIN 27
#define E0_ENABLE_PIN 24

#define TEMP_0_PIN 1
#define TEMP_1_PIN -1
#define TEMP_2_PIN -1
#define TEMP_BED_PIN 2

#define HEATER_0_PIN 4
#define HEATER_1_PIN -1
#define HEATER_2_PIN -1
#define HEATER_BED_PIN 3

#define KILL_PIN -1

#define SDPOWER -1
#define SDSS -1 // SCL pin of I2C header
#define LED_PIN -1

#if GEN7_VERSION_GE_13
// Gen7 v1.3 removed the fan pin
#define FAN_PIN -1
#else //GEN7_VERSION_GE_13
#define FAN_PIN 31
#endif //GEN7_VERSION_GE_13
#define PS_ON_PIN 15

//All these generations of Gen7 supply thermistor power
//via PS_ON, so ignore bad thermistor readings
#define BOGUS_TEMPERATURE_FAILSAFE_OVERRIDE

//our pin for debugging.
#define DEBUG_PIN 0

//our RS485 pins
#define TX_ENABLE_PIN 12
#define RX_ENABLE_PIN 13

#endif //MOTHERBOARD_EQ_11

/****************************************************************************************
* Gen7 v1.4 pin assignment
*
****************************************************************************************/

#if MOTHERBOARD_EQ_13
#define GEN7_VERSION 14 // v1.4
#endif //MOTHERBOARD_EQ_13

#if MOTHERBOARD_EQ_13
#define KNOWN_BOARD

#if (!defined(__AVR_ATmega644P__) && (!defined(__AVR_ATmega644__) && !defined(__AVR_ATmega1284P__)))
#error Oops! Make sure you have 'Gen7' selected from the 'Tools -> Boards' menu.

#endif //(!defined(__AVR_ATmega644P__) && (!defined(__AVR_ATmega644__) && !defined(__AVR_ATmega1284P__)))

#if !defined(GEN7_VERSION)
#define GEN7_VERSION 14 // v1.x
#endif //!defined(GEN7_VERSION)

//x axis pins
#define X_STEP_PIN 29
#define X_DIR_PIN 28
#define X_ENABLE_PIN 25
#define X_STOP_PIN 0

//y axis pins
#define Y_STEP_PIN 27
#define Y_DIR_PIN 26
#define Y_ENABLE_PIN 25
#define Y_STOP_PIN 1

//z axis pins
#define Z_STEP_PIN 23
#define Z_DIR_PIN 22
#define Z_ENABLE_PIN 25
#define Z_STOP_PIN 2

//extruder pins
#define E0_STEP_PIN 19
#define E0_DIR_PIN 18
#define E0_ENABLE_PIN 25

#define TEMP_0_PIN 1
#define TEMP_1_PIN -1
#define TEMP_2_PIN -1
#define TEMP_BED_PIN 0

#define HEATER_0_PIN 4
#define HEATER_1_PIN -1
#define HEATER_2_PIN -1
#define HEATER_BED_PIN 3

#define KILL_PIN -1

#define SDPOWER -1
#define SDSS -1 // SCL pin of I2C header
#define LED_PIN -1

#define FAN_PIN -1

#define PS_ON_PIN 15

//our pin for debugging.
#define DEBUG_PIN 0

//our RS485 pins
#define TX_ENABLE_PIN 12
#define RX_ENABLE_PIN 13

#endif //MOTHERBOARD_EQ_13

/*******************************************************************************
*********
* Gen7 Alfons3  pin assignment
*
********************************************************************************
********/
/* These Pins are assigned for the modified GEN7 Board from Alfons3 Please review the pins and adjust it for your needs*/

#if MOTHERBOARD_EQ_10
#define KNOWN_BOARD

#if (!defined(__AVR_ATmega644P__) && (!defined(__AVR_ATmega644__) && !defined(__AVR_ATmega1284P__)))
    #error Oops!  Make sure you have 'Gen7' selected from the 'Tools -> Boards' menu.

#endif //(!defined(__AVR_ATmega644P__) && (!defined(__AVR_ATmega644__) && !defined(__AVR_ATmega1284P__)))

//x axis pins
    #define X_STEP_PIN      21                  //different from stanard GEN7
    #define X_DIR_PIN       20				    //different from stanard GEN7
    #define X_ENABLE_PIN    24
    #define X_STOP_PIN      0

    //y axis pins
    #define Y_STEP_PIN      23
    #define Y_DIR_PIN       22
    #define Y_ENABLE_PIN    24
    #define Y_STOP_PIN      1

    //z axis pins
    #define Z_STEP_PIN      26
    #define Z_DIR_PIN       25
    #define Z_ENABLE_PIN    24
    #define Z_STOP_PIN      2

    //extruder pins
    #define E0_STEP_PIN      28
    #define E0_DIR_PIN       27
    #define E0_ENABLE_PIN    24
    
    #define TEMP_0_PIN      2
    #define TEMP_1_PIN      -1
    #define TEMP_2_PIN      -1
    #define TEMP_BED_PIN        1   // MUST USE ANALOG INPUT NUMBERING NOT DIGITAL OUTPUT NUMBERING!!!!!!!!! (pin 34 bed)
     
    #define HEATER_0_PIN    4
    #define HEATER_1_PIN    -1   
    #define HEATER_2_PIN    -1
    #define HEATER_BED_PIN      3  // (bed)

    #define SDPOWER         -1
    #define SDSS            31                  // SCL pin of I2C header || CS Pin for SD Card support
    #define LED_PIN         -1

    #define FAN_PIN         -1
    #define PS_ON_PIN       19
    //our pin for debugging.

    #define DEBUG_PIN        -1

    //our RS485 pins
    //#define TX_ENABLE_PIN       12
    //#define RX_ENABLE_PIN       13
    
    #define BEEPER -1	
	#define SDCARDDETECT -1 		
    #define SUICIDE_PIN -1						//has to be defined; otherwise Power_off doesn't work
	
    #define KILL_PIN -1
	//Pins for 4bit LCD Support 
    #define LCD_PINS_RS 18 
    #define LCD_PINS_ENABLE 17
    #define LCD_PINS_D4 16
    #define LCD_PINS_D5 15 
    #define LCD_PINS_D6 13
    #define LCD_PINS_D7 14
    
     //buttons are directly attached
    #define BTN_EN1 11
    #define BTN_EN2 10
    #define BTN_ENC 12  //the click
    
    #define BLEN_C 2
    #define BLEN_B 1
    #define BLEN_A 0

    #define encrot0 0
    #define encrot1 2
    #define encrot2 3
    #define encrot3 1
#endif //MOTHERBOARD_EQ_10

/****************************************************************************************
* Arduino Mega pin assignment
*
****************************************************************************************/
#if (MOTHERBOARD_EQ_3 || (MOTHERBOARD_EQ_33 || MOTHERBOARD_EQ_34))
#define KNOWN_BOARD 1

//////////////////FIX THIS//////////////
#if !defined(__AVR_ATmega1280__)
#if !defined(__AVR_ATmega2560__)
 #error Oops!  Make sure you have 'Arduino Mega' selected from the 'Tools -> Boards' menu.
#endif //!defined(__AVR_ATmega2560__)
#endif //!defined(__AVR_ATmega1280__)

// uncomment one of the following lines for RAMPS v1.3 or v1.0, comment both for v1.2 or 1.1
// #define RAMPS_V_1_3
// #define RAMPS_V_1_0

#if (MOTHERBOARD_EQ_33 || MOTHERBOARD_EQ_34)

#define LARGE_FLASH        true

#define X_STEP_PIN         54
#define X_DIR_PIN          55
#define X_ENABLE_PIN       38
#define X_MIN_PIN           3
#define X_MAX_PIN           2

#define Y_STEP_PIN         60
#define Y_DIR_PIN          61
#define Y_ENABLE_PIN       56
#define Y_MIN_PIN          14
#define Y_MAX_PIN          15

#define Z_STEP_PIN         46
#define Z_DIR_PIN          48
#define Z_ENABLE_PIN       62
#define Z_MIN_PIN          18
#define Z_MAX_PIN          19

#define Z2_STEP_PIN        36
#define Z2_DIR_PIN         34
#define Z2_ENABLE_PIN      30

#define E0_STEP_PIN        26
#define E0_DIR_PIN         28
#define E0_ENABLE_PIN      24

#define E1_STEP_PIN        36
#define E1_DIR_PIN         34
#define E1_ENABLE_PIN      30

#define SDPOWER            -1
#define SDSS               53
#define LED_PIN            13

#if MOTHERBOARD_EQ_33
#define FAN_PIN            9 // (Sprinter config)
#else //MOTHERBOARD_EQ_33
#define FAN_PIN            4 // IO pin. Buffer needed
#endif //MOTHERBOARD_EQ_33
#define PS_ON_PIN          12

#if (defined(REPRAP_DISCOUNT_SMART_CONTROLLER) || defined(G3D_PANEL))
#define KILL_PIN           41
#else //(defined(REPRAP_DISCOUNT_SMART_CONTROLLER) || defined(G3D_PANEL))
#define KILL_PIN           -1
#endif //(defined(REPRAP_DISCOUNT_SMART_CONTROLLER) || defined(G3D_PANEL))

#define HEATER_0_PIN       10   // EXTRUDER 1
#if MOTHERBOARD_EQ_33
#define HEATER_1_PIN       -1
#else //MOTHERBOARD_EQ_33
#define HEATER_1_PIN       9    // EXTRUDER 2 (FAN On Sprinter)
#endif //MOTHERBOARD_EQ_33
#define HEATER_2_PIN       -1   
#define TEMP_0_PIN         13   // ANALOG NUMBERING
#define TEMP_1_PIN         15   // ANALOG NUMBERING
#define TEMP_2_PIN         -1   // ANALOG NUMBERING
#define HEATER_BED_PIN     8    // BED
#define TEMP_BED_PIN       14   // ANALOG NUMBERING

#if defined(ULTRA_LCD)

#if defined(NEWPANEL)
     //encoder rotation values
    #define encrot0 0
    #define encrot1 2
    #define encrot2 3
    #define encrot3 1

    #define BLEN_A 0
    #define BLEN_B 1
    #define BLEN_C 2

    #define LCD_PINS_RS 16 
    #define LCD_PINS_ENABLE 17
    #define LCD_PINS_D4 23
    #define LCD_PINS_D5 25 
    #define LCD_PINS_D6 27
    #define LCD_PINS_D7 29
    
#if defined(REPRAP_DISCOUNT_SMART_CONTROLLER)
      #define BEEPER 37

      #define BTN_EN1 31
      #define BTN_EN2 33
      #define BTN_ENC 35

      #define SDCARDDETECT 49
#else //defined(REPRAP_DISCOUNT_SMART_CONTROLLER)
      //arduino pin which triggers an piezzo beeper
      #define BEEPER 33	 // Beeper on AUX-4

      //buttons are directly attached using AUX-2
#if defined(REPRAPWORLD_KEYPAD)
        #define BTN_EN1 64 // encoder
        #define BTN_EN2 59 // encoder
        #define BTN_ENC 63 // enter button
        #define SHIFT_OUT 40 // shift register
        #define SHIFT_CLK 44 // shift register
        #define SHIFT_LD 42 // shift register
        // define register bit values, don't change it
        #define BLEN_REPRAPWORLD_KEYPAD_F3 0
        #define BLEN_REPRAPWORLD_KEYPAD_F2 1
        #define BLEN_REPRAPWORLD_KEYPAD_F1 2
        #define BLEN_REPRAPWORLD_KEYPAD_UP 3
        #define BLEN_REPRAPWORLD_KEYPAD_RIGHT 4
        #define BLEN_REPRAPWORLD_KEYPAD_MIDDLE 5
        #define BLEN_REPRAPWORLD_KEYPAD_DOWN 6
        #define BLEN_REPRAPWORLD_KEYPAD_LEFT 7
#else //defined(REPRAPWORLD_KEYPAD)
        #define BTN_EN1 37
        #define BTN_EN2 35
        #define BTN_ENC 31  //the click
#endif //defined(REPRAPWORLD_KEYPAD)

#if defined(G3D_PANEL)
        #define SDCARDDETECT 49
#else //defined(G3D_PANEL)
        #define SDCARDDETECT -1  // Ramps does not use this port
#endif //defined(G3D_PANEL)
#endif //defined(REPRAP_DISCOUNT_SMART_CONTROLLER)

#else //defined(NEWPANEL)
    //arduino pin witch triggers an piezzo beeper
    #define BEEPER 33		No Beeper added

    //buttons are attached to a shift register
	// Not wired this yet
    //#define SHIFT_CLK 38
    //#define SHIFT_LD 42
    //#define SHIFT_OUT 40
    //#define SHIFT_EN 17
    
    #define LCD_PINS_RS 16 
    #define LCD_PINS_ENABLE 17
    #define LCD_PINS_D4 23
    #define LCD_PINS_D5 25 
    #define LCD_PINS_D6 27
    #define LCD_PINS_D7 29
    
    //encoder rotation values
    #define encrot0 0
    #define encrot1 2
    #define encrot2 3
    #define encrot3 1

    
    //bits in the shift register that carry the buttons for:
    // left up center down right red
    #define BL_LE 7
    #define BL_UP 6
    #define BL_MI 5
    #define BL_DW 4
    #define BL_RI 3
    #define BL_ST 2

    #define BLEN_B 1
    #define BLEN_A 0
#endif //defined(NEWPANEL)
#endif //defined(ULTRA_LCD)

#else //(MOTHERBOARD_EQ_33 || MOTHERBOARD_EQ_34)

#define X_STEP_PIN         26
#define X_DIR_PIN          28
#define X_ENABLE_PIN       24
#define X_MIN_PIN           3
#define X_MAX_PIN          -1    //2

#define Y_STEP_PIN         38
#define Y_DIR_PIN          40
#define Y_ENABLE_PIN       36
#define Y_MIN_PIN          16
#define Y_MAX_PIN          -1    //17

#define Z_STEP_PIN         44
#define Z_DIR_PIN          46
#define Z_ENABLE_PIN       42
#define Z_MIN_PIN          18
#define Z_MAX_PIN          -1    //19

#define E0_STEP_PIN         32
#define E0_DIR_PIN          34
#define E0_ENABLE_PIN       30

#define SDPOWER            48
#define SDSS               53
#define LED_PIN            13
#define PS_ON_PIN          -1
#define KILL_PIN           -1

#if defined(RAMPS_V_1_0 )
  #define HEATER_0_PIN     12    // RAMPS 1.0
  #define HEATER_BED_PIN   -1    // RAMPS 1.0
  #define FAN_PIN          11    // RAMPS 1.0
#else //defined(RAMPS_V_1_0 )
  #define HEATER_0_PIN     10    // RAMPS 1.1
  #define HEATER_BED_PIN    8    // RAMPS 1.1
  #define FAN_PIN           9    // RAMPS 1.1
#endif //defined(RAMPS_V_1_0 )
#define HEATER_1_PIN        -1
#define HEATER_2_PIN        -1
#define TEMP_0_PIN          2    // MUST USE ANALOG INPUT NUMBERING NOT DIGITAL OUTPUT NUMBERING!!!!!!!!!
#define TEMP_1_PIN          -1   
#define TEMP_2_PIN          -1   
#define TEMP_BED_PIN        1    // MUST USE ANALOG INPUT NUMBERING NOT DIGITAL OUTPUT NUMBERING!!!!!!!!!
#endif //(MOTHERBOARD_EQ_33 || MOTHERBOARD_EQ_34)

// SPI for Max6675 Thermocouple 

#if !defined(SDSUPPORT)
// these pins are defined in the SD library if building with SD support  
  #define MAX_SCK_PIN          52
  #define MAX_MISO_PIN         50
  #define MAX_MOSI_PIN         51
  #define MAX6675_SS       53
#else //!defined(SDSUPPORT)
  #define MAX6675_SS       49
#endif //!defined(SDSUPPORT)

#endif //(MOTHERBOARD_EQ_3 || (MOTHERBOARD_EQ_33 || MOTHERBOARD_EQ_34))

/****************************************************************************************
* Duemilanove w/ ATMega328P pin assignment
*
****************************************************************************************/
#if MOTHERBOARD_EQ_4
#define KNOWN_BOARD 1

#if !defined(__AVR_ATmega328P__)
#error Oops!  Make sure you have 'Arduino Duemilanove w/ ATMega328' selected from the 'Tools -> Boards' menu.
#endif //!defined(__AVR_ATmega328P__)

#define X_STEP_PIN         19
#define X_DIR_PIN          18
#define X_ENABLE_PIN       -1
#define X_STOP_PIN         17

#define Y_STEP_PIN         10
#define Y_DIR_PIN           7
#define Y_ENABLE_PIN       -1
#define Y_STOP_PIN          8

#define Z_STEP_PIN         13
#define Z_DIR_PIN           3
#define Z_ENABLE_PIN        2
#define Z_STOP_PIN          4

#define E0_STEP_PIN         11
#define E0_DIR_PIN          12
#define E0_ENABLE_PIN       -1

#define SDPOWER          -1
#define SDSS          -1
#define LED_PIN            -1
#define FAN_PIN             5
#define PS_ON_PIN          -1
#define KILL_PIN           -1

#define HEATER_0_PIN        6
#define HEATER_1_PIN        -1
#define HEATER_2_PIN        -1
#define TEMP_0_PIN          0    // MUST USE ANALOG INPUT NUMBERING NOT DIGITAL OUTPUT NUMBERING!!!!!!!!!
#define TEMP_1_PIN          -1    
#define TEMP_2_PIN          -1    
#define HEATER_BED_PIN      -1
#define TEMP_BED_PIN        -1

#endif //MOTHERBOARD_EQ_4

/****************************************************************************************
* Gen6 pin assignment
*
****************************************************************************************/
#if (MOTHERBOARD_EQ_5 || MOTHERBOARD_EQ_51)
#define KNOWN_BOARD 1

#if !defined(__AVR_ATmega644P__)
#if !defined(__AVR_ATmega1284P__)
#error Oops!  Make sure you have 'Sanguino' selected from the 'Tools -> Boards' menu.
#endif //!defined(__AVR_ATmega1284P__)
#endif //!defined(__AVR_ATmega644P__)

//x axis pins
    #define X_STEP_PIN      15
    #define X_DIR_PIN       18
    #define X_ENABLE_PIN    19
    #define X_STOP_PIN      20

    //y axis pins
    #define Y_STEP_PIN      23
    #define Y_DIR_PIN       22
    #define Y_ENABLE_PIN    24
    #define Y_STOP_PIN      25

    //z axis pins
    #define Z_STEP_PIN      27
    #define Z_DIR_PIN       28
    #define Z_ENABLE_PIN    29
    #define Z_STOP_PIN      30

    //extruder pins
    #define E0_STEP_PIN      4    //Edited @ EJE Electronics 20100715
    #define E0_DIR_PIN       2    //Edited @ EJE Electronics 20100715
    #define E0_ENABLE_PIN    3    //Added @ EJE Electronics 20100715
    #define TEMP_0_PIN      5     //changed @ rkoeppl 20110410
    #define TEMP_1_PIN      -1    //changed @ rkoeppl 20110410


    #define TEMP_2_PIN      -1    //changed @ rkoeppl 20110410
    #define HEATER_0_PIN    14    //changed @ rkoeppl 20110410
    #define HEATER_1_PIN    -1
    #define HEATER_2_PIN    -1
#if MOTHERBOARD_EQ_5
    #define HEATER_BED_PIN  -1    //changed @ rkoeppl 20110410
    #define TEMP_BED_PIN    -1    //changed @ rkoeppl 20110410
#else //MOTHERBOARD_EQ_5
    #define HEATER_BED_PIN   1    //changed @ rkoeppl 20110410
    #define TEMP_BED_PIN     0    //changed @ rkoeppl 20110410
#endif //MOTHERBOARD_EQ_5
    #define SDPOWER          -1
    #define SDSS          17
    #define LED_PIN         -1    //changed @ rkoeppl 20110410
    #define FAN_PIN         -1    //changed @ rkoeppl 20110410
    #define PS_ON_PIN       -1    //changed @ rkoeppl 20110410
    #define KILL_PIN        -1    //changed @ drakelive 20120830
    //our pin for debugging.
    
    #define DEBUG_PIN        0
    
    //our RS485 pins
    #define TX_ENABLE_PIN	12
    #define RX_ENABLE_PIN	13

    
#endif //(MOTHERBOARD_EQ_5 || MOTHERBOARD_EQ_51)

/****************************************************************************************
* Sanguinololu pin assignment
*
****************************************************************************************/
#if MOTHERBOARD_EQ_64
#define STB
#endif //MOTHERBOARD_EQ_64
#if MOTHERBOARD_EQ_63
#define MELZI
#endif //MOTHERBOARD_EQ_63
#if (MOTHERBOARD_EQ_62 || (MOTHERBOARD_EQ_63 || MOTHERBOARD_EQ_64))
#undef MOTHERBOARD
#define MOTHERBOARD 6
#define SANGUINOLOLU_V_1_2 
#endif //(MOTHERBOARD_EQ_62 || (MOTHERBOARD_EQ_63 || MOTHERBOARD_EQ_64))
#if MOTHERBOARD_EQ_6
#define KNOWN_BOARD 1
#if !defined(__AVR_ATmega644P__)
#if !defined(__AVR_ATmega1284P__)
#error Oops!  Make sure you have 'Sanguino' selected from the 'Tools -> Boards' menu.
#endif //!defined(__AVR_ATmega1284P__)
#endif //!defined(__AVR_ATmega644P__)

#define X_STEP_PIN         15
#define X_DIR_PIN          21
#define X_STOP_PIN         18

#define Y_STEP_PIN         22
#define Y_DIR_PIN          23
#define Y_STOP_PIN         19

#define Z_STEP_PIN         3
#define Z_DIR_PIN          2
#define Z_STOP_PIN         20

#define E0_STEP_PIN         1
#define E0_DIR_PIN          0

#define LED_PIN            -1

#define FAN_PIN            -1 
#if (FAN_PIN_EQ_12 || FAN_PIN_EQ_13)
#define FAN_SOFT_PWM
#endif //(FAN_PIN_EQ_12 || FAN_PIN_EQ_13)

#if defined(MELZI)
#define LED_PIN            27 /* On some broken versions of the Sanguino libraries the pin definitions are wrong, which then needs LED_PIN as pin 28. But you better upgrade your Sanguino libraries! See #368. */
#define FAN_PIN            4
#endif //defined(MELZI)

#if defined(STB)
#define FAN_PIN            4
#endif //defined(STB)

#define PS_ON_PIN          -1
#define KILL_PIN           -1

#define HEATER_0_PIN       13 // (extruder)
#define HEATER_1_PIN       -1
#define HEATER_2_PIN       -1

#if defined(SANGUINOLOLU_V_1_2)

#define HEATER_BED_PIN     12 // (bed)
#define X_ENABLE_PIN       14
#define Y_ENABLE_PIN       14
#define Z_ENABLE_PIN       26
#define E0_ENABLE_PIN       14

#else //defined(SANGUINOLOLU_V_1_2)

#define HEATER_BED_PIN       14  // (bed)
#define X_ENABLE_PIN       -1
#define Y_ENABLE_PIN       -1
#define Z_ENABLE_PIN       -1
#define E0_ENABLE_PIN       -1

#endif //defined(SANGUINOLOLU_V_1_2)

#define TEMP_0_PIN          7   // MUST USE ANALOG INPUT NUMBERING NOT DIGITAL OUTPUT NUMBERING!!!!!!!!! (pin 33 extruder)
#define TEMP_1_PIN         -1
#define TEMP_2_PIN         -1
#define TEMP_BED_PIN        6   // MUST USE ANALOG INPUT NUMBERING NOT DIGITAL OUTPUT NUMBERING!!!!!!!!! (pin 34 bed)
#define SDPOWER            -1
#define SDSS               31

/* On some broken versions of the Sanguino libraries the pin definitions are wrong, which then needs SDSS as pin 24. But you better upgrade your Sanguino libraries! See #368. */
//#define SDSS               24

#if defined(ULTRA_LCD)
#if defined(NEWPANEL)
     //we have no buzzer installed
     #define BEEPER -1
     //LCD Pins
#if defined(DOGLCD)
			 // Pins for DOGM SPI LCD Support
			 #define DOGLCD_A0	30
			 #define DOGLCD_CS	29
			 // GLCD features
			 #define LCD_CONTRAST 1
			 // Uncomment screen orientation
		     // #define LCD_SCREEN_ROT_0
		     // #define LCD_SCREEN_ROT_90
			 #define LCD_SCREEN_ROT_180
		     // #define LCD_SCREEN_ROT_270
#else //defined(DOGLCD)
			 #define LCD_PINS_RS        4
			 #define LCD_PINS_ENABLE    17
			 #define LCD_PINS_D4        30
			 #define LCD_PINS_D5        29
			 #define LCD_PINS_D6        28
			 #define LCD_PINS_D7        27
#endif //defined(DOGLCD)
     //The encoder and click button
     #define BTN_EN1 11  //must be a hardware interrupt pin
     #define BTN_EN2 10 //must be hardware interrupt pin
     #define BTN_ENC 16  //the switch
     //not connected to a pin
     #define SDCARDDETECT -1
     
     //from the same bit in the RAMPS Newpanel define
     //encoder rotation values
     #define encrot0 0
     #define encrot1 2
     #define encrot2 3
     #define encrot3 1
     
     #define BLEN_C 2
     #define BLEN_B 1
     #define BLEN_A 0
     
#endif //defined(NEWPANEL)
#endif //defined(ULTRA_LCD)
 
#endif //MOTHERBOARD_EQ_6


#if MOTHERBOARD_EQ_7
#define KNOWN_BOARD
/*****************************************************************
* Ultimaker pin assignment
******************************************************************/

#if !defined(__AVR_ATmega1280__)
#if !defined(__AVR_ATmega2560__)
 #error Oops!  Make sure you have 'Arduino Mega' selected from the 'Tools -> Boards' menu.
#endif //!defined(__AVR_ATmega2560__)
#endif //!defined(__AVR_ATmega1280__)

#define LARGE_FLASH true

#define X_STEP_PIN 25
#define X_DIR_PIN 23
#define X_MIN_PIN 22
#define X_MAX_PIN 24
#define X_ENABLE_PIN 27

#define Y_STEP_PIN 31
#define Y_DIR_PIN 33
#define Y_MIN_PIN 26
#define Y_MAX_PIN 28
#define Y_ENABLE_PIN 29

#define Z_STEP_PIN 37 
#define Z_DIR_PIN 39
#define Z_MIN_PIN 30
#define Z_MAX_PIN 32
#define Z_ENABLE_PIN 35

#define HEATER_BED_PIN 4 
#define TEMP_BED_PIN 10  

#define HEATER_0_PIN  2
#define TEMP_0_PIN 8   

#define HEATER_1_PIN 3
#define TEMP_1_PIN 9

#define HEATER_2_PIN -1
#define TEMP_2_PIN -1

#define E0_STEP_PIN         43
#define E0_DIR_PIN          45
#define E0_ENABLE_PIN       41

#define E1_STEP_PIN         49
#define E1_DIR_PIN          47
#define E1_ENABLE_PIN       48

#define SDPOWER            -1
#define SDSS               53
#define LED_PIN            13
#define FAN_PIN            7
#define PS_ON_PIN          12
#define KILL_PIN           -1
#define SUICIDE_PIN        54  //PIN that has to be turned on right after start, to keep power flowing.

#if defined(ULTRA_LCD)

#if defined(NEWPANEL)
  //arduino pin witch triggers an piezzo beeper
    #define BEEPER 18

    #define LCD_PINS_RS 20 
    #define LCD_PINS_ENABLE 17
    #define LCD_PINS_D4 16
    #define LCD_PINS_D5 21 
    #define LCD_PINS_D6 5
    #define LCD_PINS_D7 6
    
    //buttons are directly attached
    #define BTN_EN1 40
    #define BTN_EN2 42
    #define BTN_ENC 19  //the click
    
    #define BLEN_C 2
    #define BLEN_B 1
    #define BLEN_A 0
    
    #define SDCARDDETECT 38
    
      //encoder rotation values
    #define encrot0 0
    #define encrot1 2
    #define encrot2 3
    #define encrot3 1
#else //defined(NEWPANEL)
    //arduino pin witch triggers an piezzo beeper
    #define BEEPER 18

    //buttons are attached to a shift register
    #define SHIFT_CLK 38
    #define SHIFT_LD 42
    #define SHIFT_OUT 40
    #define SHIFT_EN 17
    
    #define LCD_PINS_RS 16 
    #define LCD_PINS_ENABLE 5
    #define LCD_PINS_D4 6
    #define LCD_PINS_D5 21 
    #define LCD_PINS_D6 20
    #define LCD_PINS_D7 19
    
    //encoder rotation values
#if !defined(ULTIMAKERCONTROLLER)
     #define encrot0 0
     #define encrot1 2
     #define encrot2 3
     #define encrot3 1
#else //!defined(ULTIMAKERCONTROLLER)
     #define encrot0 0
     #define encrot1 1
     #define encrot2 3
     #define encrot3 2

#endif //!defined(ULTIMAKERCONTROLLER)

    #define SDCARDDETECT -1
    //bits in the shift register that carry the buttons for:
    // left up center down right red
    #define BL_LE 7
    #define BL_UP 6
    #define BL_MI 5
    #define BL_DW 4
    #define BL_RI 3
    #define BL_ST 2

    #define BLEN_B 1
    #define BLEN_A 0
#endif //defined(NEWPANEL)
#endif //defined(ULTRA_LCD)

#endif //MOTHERBOARD_EQ_7

#if MOTHERBOARD_EQ_71
#define KNOWN_BOARD
/*****************************************************************
* Ultimaker pin assignment (Old electronics)
******************************************************************/

#if !defined(__AVR_ATmega1280__)
#if !defined(__AVR_ATmega2560__)
 #error Oops!  Make sure you have 'Arduino Mega' selected from the 'Tools -> Boards' menu.
#endif //!defined(__AVR_ATmega2560__)
#endif //!defined(__AVR_ATmega1280__)

#define LARGE_FLASH true

#define X_STEP_PIN 25
#define X_DIR_PIN 23
#define X_MIN_PIN 15
#define X_MAX_PIN 14
#define X_ENABLE_PIN 27

#define Y_STEP_PIN 31
#define Y_DIR_PIN 33
#define Y_MIN_PIN 17
#define Y_MAX_PIN 16
#define Y_ENABLE_PIN 29

#define Z_STEP_PIN 37 
#define Z_DIR_PIN 39
#define Z_MIN_PIN 19
#define Z_MAX_PIN 18
#define Z_ENABLE_PIN 35

#define HEATER_BED_PIN -1 
#define TEMP_BED_PIN -1  

#define HEATER_0_PIN  2
#define TEMP_0_PIN 8   

#define HEATER_1_PIN 1
#define TEMP_1_PIN 1

#define HEATER_2_PIN -1
#define TEMP_2_PIN -1

#define E0_STEP_PIN         43
#define E0_DIR_PIN          45
#define E0_ENABLE_PIN       41

#define E1_STEP_PIN         -1
#define E1_DIR_PIN          -1
#define E1_ENABLE_PIN       -1

#define SDPOWER            -1
#define SDSS               -1
#define LED_PIN            -1
#define FAN_PIN            -1
#define PS_ON_PIN          -1
#define KILL_PIN           -1
#define SUICIDE_PIN        -1  //PIN that has to be turned on right after start, to keep power flowing.

#define LCD_PINS_RS 24 
#define LCD_PINS_ENABLE 22
#define LCD_PINS_D4 36
#define LCD_PINS_D5 34 
#define LCD_PINS_D6 32
#define LCD_PINS_D7 30

#endif //MOTHERBOARD_EQ_71


/****************************************************************************************
* RUMBA pin assignment
*
****************************************************************************************/
#if MOTHERBOARD_EQ_80
#define KNOWN_BOARD 1

#if !defined(__AVR_ATmega2560__)
 #error Oops!  Make sure you have 'Arduino Mega' selected from the 'Tools -> Boards' menu.
#endif //!defined(__AVR_ATmega2560__)

#define X_STEP_PIN         17
#define X_DIR_PIN          16
#define X_ENABLE_PIN       48
#define X_MIN_PIN          37
#define X_MAX_PIN          36 

#define Y_STEP_PIN         54
#define Y_DIR_PIN          47 
#define Y_ENABLE_PIN       55
#define Y_MIN_PIN          35
#define Y_MAX_PIN          34 

#define Z_STEP_PIN         57 
#define Z_DIR_PIN          56
#define Z_ENABLE_PIN       62 
#define Z_MIN_PIN          33
#define Z_MAX_PIN          32

#define E0_STEP_PIN        23
#define E0_DIR_PIN         22
#define E0_ENABLE_PIN      24

#define E1_STEP_PIN        26
#define E1_DIR_PIN         25
#define E1_ENABLE_PIN      27

#define E2_STEP_PIN        29
#define E2_DIR_PIN         28
#define E2_ENABLE_PIN      39

#define LED_PIN            13

#define FAN_PIN            7 
//additional FAN1 PIN (e.g. useful for electronics fan or light on/off) on PIN 8

#define PS_ON_PIN          45
#define KILL_PIN           46

#define HEATER_0_PIN       2    // EXTRUDER 1
#define HEATER_1_PIN       3    // EXTRUDER 2
#define HEATER_2_PIN       6    // EXTRUDER 3
//optional FAN1 can be used as 4th heater output: #define HEATER_3_PIN       8    // EXTRUDER 4
#define HEATER_BED_PIN     9    // BED

#define TEMP_0_PIN         15   // ANALOG NUMBERING
#define TEMP_1_PIN         14   // ANALOG NUMBERING
#define TEMP_2_PIN         13   // ANALOG NUMBERING
//optional for extruder 4 or chamber: #define TEMP_2_PIN         12   // ANALOG NUMBERING
#define TEMP_BED_PIN       11   // ANALOG NUMBERING

#define SDPOWER            -1
#define SDSS               53
#define SDCARDDETECT       49
#define BEEPER             44
#define LCD_PINS_RS        19 
#define LCD_PINS_ENABLE    42
#define LCD_PINS_D4        18
#define LCD_PINS_D5        38 
#define LCD_PINS_D6        41
#define LCD_PINS_D7        40
#define BTN_EN1            11
#define BTN_EN2            12
#define BTN_ENC            43
//encoder rotation values
#define BLEN_C 2
#define BLEN_B 1
#define BLEN_A 0
#define encrot0 0
#define encrot1 2
#define encrot2 3
#define encrot3 1

#endif //MOTHERBOARD_EQ_80


/****************************************************************************************
* Teensylu 0.7 / Printrboard pin assignments (AT90USB1286)
* Requires the Teensyduino software with Teensy++ 2.0 selected in Arduino IDE!
  http://www.pjrc.com/teensy/teensyduino.html
* See http://reprap.org/wiki/Printrboard for more info
****************************************************************************************/
#if (MOTHERBOARD_EQ_8 || MOTHERBOARD_EQ_81)
#define KNOWN_BOARD 1
#define AT90USB 1286  // Disable MarlinSerial etc.

#if !defined(__AVR_AT90USB1286__)
#error Oops!  Make sure you have 'Teensy++ 2.0' selected from the 'Tools -> Boards' menu.
#endif //!defined(__AVR_AT90USB1286__)

#define LARGE_FLASH        true

#define X_STEP_PIN          0
#define X_DIR_PIN           1
#define X_ENABLE_PIN       39

#define Y_STEP_PIN          2
#define Y_DIR_PIN           3
#define Y_ENABLE_PIN       38

#define Z_STEP_PIN          4
#define Z_DIR_PIN           5
#define Z_ENABLE_PIN       23

#define E0_STEP_PIN         6
#define E0_DIR_PIN          7
#define E0_ENABLE_PIN      19

#define HEATER_0_PIN       21  // Extruder
#define HEATER_1_PIN       -1
#define HEATER_2_PIN       -1
#define HEATER_BED_PIN     20  // Bed
#define FAN_PIN            22  // Fan
// You may need to change FAN_PIN to 16 because Marlin isn't using fastio.h
// for the fan and Teensyduino uses a different pin mapping.

#if MOTHERBOARD_EQ_8
  #define X_STOP_PIN         13
  #define Y_STOP_PIN         14
  #define Z_STOP_PIN         15
  #define TEMP_0_PIN          7  // Extruder / Analog pin numbering
  #define TEMP_BED_PIN        6  // Bed / Analog pin numbering
#else //MOTHERBOARD_EQ_8
  #define X_STOP_PIN         35
  #define Y_STOP_PIN          8
  #define Z_STOP_PIN         36
  #define TEMP_0_PIN          1  // Extruder / Analog pin numbering
  #define TEMP_BED_PIN        0  // Bed / Analog pin numbering
#endif //MOTHERBOARD_EQ_8

#define TEMP_1_PIN         -1
#define TEMP_2_PIN         -1

#define SDPOWER            -1
#define SDSS                8
#define LED_PIN            -1
#define PS_ON_PIN          -1
#define KILL_PIN           -1
#define ALARM_PIN          -1

#if !defined(SDSUPPORT)
// these pins are defined in the SD library if building with SD support
  #define SCK_PIN           9
  #define MISO_PIN         11
  #define MOSI_PIN         10
#endif //!defined(SDSUPPORT)

#endif //(MOTHERBOARD_EQ_8 || MOTHERBOARD_EQ_81)

/****************************************************************************************
 * Brainwave 1.0 pin assignments (AT90USB646)
 * Requires hardware bundle for Arduino:
   https://github.com/unrepentantgeek/brainwave-arduino
 ****************************************************************************************/
#if MOTHERBOARD_EQ_82
#define KNOWN_BOARD 1
#define AT90USB 646  // Disable MarlinSerial etc.

#if !defined(__AVR_AT90USB646__)
#error Oops!  Make sure you have 'Brainwave' selected from the 'Tools -> Boards' menu.
#endif //!defined(__AVR_AT90USB646__)

#define X_STEP_PIN         27
#define X_DIR_PIN          29
#define X_ENABLE_PIN       28
#define X_STOP_PIN          7
#define X_ATT_PIN          26

#define Y_STEP_PIN         31
#define Y_DIR_PIN          33
#define Y_ENABLE_PIN       32
#define Y_STOP_PIN          6
#define Y_ATT_PIN          30

#define Z_STEP_PIN         17
#define Z_DIR_PIN          19
#define Z_ENABLE_PIN       18
#define Z_STOP_PIN          5
#define Z_ATT_PIN          16

#define E0_STEP_PIN        21
#define E0_DIR_PIN         23
#define E0_ENABLE_PIN      22
#define E0_ATT_PIN         20

#define HEATER_0_PIN        4  // Extruder
#define HEATER_1_PIN       -1
#define HEATER_2_PIN       -1
#define HEATER_BED_PIN     38  // Bed
#define FAN_PIN             3  // Fan

#define TEMP_0_PIN          7  // Extruder / Analog pin numbering
#define TEMP_1_PIN         -1
#define TEMP_2_PIN         -1
#define TEMP_BED_PIN        6  // Bed / Analog pin numbering

#define SDPOWER            -1
#define SDSS               -1
#define LED_PIN            39
#define PS_ON_PIN          -1
#define KILL_PIN           -1
#define ALARM_PIN          -1

#if !defined(SDSUPPORT)
// these pins are defined in the SD library if building with SD support
  #define SCK_PIN           9
  #define MISO_PIN         11
  #define MOSI_PIN         10
#endif //!defined(SDSUPPORT)

#endif //MOTHERBOARD_EQ_82

/****************************************************************************************
* Gen3+ pin assignment
*
****************************************************************************************/
#if MOTHERBOARD_EQ_9
#define MOTHERBOARD 6   /*TODO: Figure out, Why is this done?*/
#define KNOWN_BOARD 1
#if !defined(__AVR_ATmega644P__)
#if !defined(__AVR_ATmega1284P__)
#error Oops!  Make sure you have 'Sanguino' selected from the 'Tools -> Boards' menu.
#endif //!defined(__AVR_ATmega1284P__)
#endif //!defined(__AVR_ATmega644P__)

#define X_STEP_PIN         15
#define X_DIR_PIN          18
#define X_STOP_PIN         20

#define Y_STEP_PIN         23
#define Y_DIR_PIN          22
#define Y_STOP_PIN         25

#define Z_STEP_PIN         27
#define Z_DIR_PIN          28
#define Z_STOP_PIN         30

#define E_STEP_PIN         17
#define E_DIR_PIN          21

#define LED_PIN            -1

#define FAN_PIN            -1 

#define PS_ON_PIN         14
#define KILL_PIN           -1

#define HEATER_0_PIN       12 // (extruder)

#define HEATER_1_PIN       16 // (bed)
#define X_ENABLE_PIN       19
#define Y_ENABLE_PIN       24
#define Z_ENABLE_PIN       29
#define E_ENABLE_PIN       13

#define TEMP_0_PIN          0   // MUST USE ANALOG INPUT NUMBERING NOT DIGITAL OUTPUT NUMBERING!!!!!!!!! (pin 33 extruder)
#define TEMP_1_PIN          5   // MUST USE ANALOG INPUT NUMBERING NOT DIGITAL OUTPUT NUMBERING!!!!!!!!! (pin 34 bed)
#define TEMP_2_PIN         -1
#define SDPOWER            -1
#define SDSS               4
#define HEATER_2_PIN       -1

#endif //MOTHERBOARD_EQ_9



/****************************************************************************************
* Open Motion controller with enable based extruders
*
*                        ATMega644
*
*                        +---\/---+
*            (D 0) PB0  1|        |40  PA0 (AI 0 / D31)
*            (D 1) PB1  2|        |39  PA1 (AI 1 / D30)
*       INT2 (D 2) PB2  3|        |38  PA2 (AI 2 / D29)
*        PWM (D 3) PB3  4|        |37  PA3 (AI 3 / D28)
*        PWM (D 4) PB4  5|        |36  PA4 (AI 4 / D27)
*       MOSI (D 5) PB5  6|        |35  PA5 (AI 5 / D26)
*       MISO (D 6) PB6  7|        |34  PA6 (AI 6 / D25)
*        SCK (D 7) PB7  8|        |33  PA7 (AI 7 / D24)
*                  RST  9|        |32  AREF
*                  VCC 10|        |31  GND 
*                  GND 11|        |30  AVCC
*                XTAL2 12|        |29  PC7 (D 23)
*                XTAL1 13|        |28  PC6 (D 22)
*       RX0 (D 8)  PD0 14|        |27  PC5 (D 21) TDI
*       TX0 (D 9)  PD1 15|        |26  PC4 (D 20) TDO
*  INT0 RX1 (D 10) PD2 16|        |25  PC3 (D 19) TMS
*  INT1 TX1 (D 11) PD3 17|        |24  PC2 (D 18) TCK
*       PWM (D 12) PD4 18|        |23  PC1 (D 17) SDA
*       PWM (D 13) PD5 19|        |22  PC0 (D 16) SCL
*       PWM (D 14) PD6 20|        |21  PD7 (D 15) PWM
*                        +--------+
*
****************************************************************************************/
#if MOTHERBOARD_EQ_90
#define KNOWN_BOARD 1

#if !defined(__AVR_ATmega644__)
#error Oops!  Make sure you have 'SanguinoA' selected from the 'Tools -> Boards' menu.
#endif //!defined(__AVR_ATmega644__)

#define X_STEP_PIN         21
#define X_DIR_PIN          20
#define X_ENABLE_PIN       24
#define X_STOP_PIN         0

#define Y_STEP_PIN         23
#define Y_DIR_PIN          22
#define Y_ENABLE_PIN       24
#define Y_STOP_PIN         1

#define Z_STEP_PIN         26
#define Z_DIR_PIN          25
#define Z_ENABLE_PIN       24
#define Z_STOP_PIN         2

#define E0_STEP_PIN         28
#define E0_DIR_PIN          27
#define E0_ENABLE_PIN       24

#define E1_STEP_PIN         -1 // 19
#define E1_DIR_PIN          -1 // 18
#define E1_ENABLE_PIN       24

#define E2_STEP_PIN         -1 // 17
#define E2_DIR_PIN          -1 // 16
#define E2_ENABLE_PIN       24

#define SDPOWER            -1
#define SDSS               11
#define SDCARDDETECT       -1 // 10 optional also used as mode pin
#define LED_PIN            -1
#define FAN_PIN            3
#define PS_ON_PIN          -1
#define KILL_PIN           -1

#define HEATER_0_PIN       4
#define HEATER_1_PIN       -1 // 12 
#define HEATER_2_PIN       -1 // 13
#define TEMP_0_PIN          0 //D27   // MUST USE ANALOG INPUT NUMBERING NOT DIGITAL OUTPUT NUMBERING!!!!!!!!!
#define TEMP_1_PIN         -1 // 1
#define TEMP_2_PIN         -1 // 2
#define HEATER_BED_PIN     -1 // 14/15
#define TEMP_BED_PIN       -1 // 1,2 or I2C
/*  Unused (1) (2) (3) 4 5 6 7 8 9 10 11 12 13 (14) (15) (16) 17 (18) (19) (20) (21) (22) (23) 24 (25) (26) (27) 28 (29) (30) (31)  */

#endif //MOTHERBOARD_EQ_90

#if MOTHERBOARD_EQ_91
#define KNOWN_BOARD 1

#if (!defined(__AVR_ATmega644P__) && !defined(__AVR_ATmega644__))
#error Oops!  Make sure you have 'Sanguino' selected from the 'Tools -> Boards' menu. (Final OMCA board)
#endif //(!defined(__AVR_ATmega644P__) && !defined(__AVR_ATmega644__))

#define X_STEP_PIN         26
#define X_DIR_PIN          25
#define X_ENABLE_PIN       10
#define X_STOP_PIN         0

#define Y_STEP_PIN         28
#define Y_DIR_PIN          27
#define Y_ENABLE_PIN       10
#define Y_STOP_PIN         1

#define Z_STEP_PIN         23
#define Z_DIR_PIN          22
#define Z_ENABLE_PIN       10
#define Z_STOP_PIN         2

#define E0_STEP_PIN         24
#define E0_DIR_PIN          21
#define E0_ENABLE_PIN       10

/* future proofing */
#define __FS	20
#define __FD	19
#define __GS	18
#define __GD	13

#define UNUSED_PWM           14	/* PWM on LEFT connector */

#define E1_STEP_PIN         -1 // 21
#define E1_DIR_PIN          -1 // 20
#define E1_ENABLE_PIN       -1 // 19

#define E2_STEP_PIN         -1 // 21
#define E2_DIR_PIN          -1 // 20
#define E2_ENABLE_PIN       -1 // 18

#define SDPOWER            -1
#define SDSS               11
#define SDCARDDETECT       -1 // 10 optional also used as mode pin
#define LED_PIN            -1
#define FAN_PIN            14 /* PWM on MIDDLE connector */
#define PS_ON_PIN          -1
#define KILL_PIN           -1

#define HEATER_0_PIN        3 /*DONE PWM on RIGHT connector */
#define HEATER_1_PIN       -1 
#define HEATER_2_PIN       -1
#define HEATER_1_PIN       -1 
#define HEATER_2_PIN       -1
#define TEMP_0_PIN          0 // ANALOG INPUT NUMBERING 
#define TEMP_1_PIN          1 // ANALOG
#define TEMP_2_PIN         -1 // 2
#define HEATER_BED_PIN      4
#define TEMP_BED_PIN        2 // 1,2 or I2C

#define I2C_SCL				16
#define I2C_SDA				17

#endif //MOTHERBOARD_EQ_91

#define KNOWN_BOARD
/*****************************************************************
* Rambo Pin Assignments
******************************************************************/

#if !defined(__AVR_ATmega2560__)
#error Oops!  Make sure you have 'Arduino Mega 2560' selected from the 'Tools -> Boards' menu.
#endif //!defined(__AVR_ATmega2560__)

#define LARGE_FLASH true

#define X_STEP_PIN 37
#define X_DIR_PIN 48
#define X_MIN_PIN 12
#define X_MAX_PIN 24
#define X_ENABLE_PIN 29
#define X_MS1_PIN 40
#define X_MS2_PIN 41

#define Y_STEP_PIN 36
#define Y_DIR_PIN 49
#define Y_MIN_PIN 11
#define Y_MAX_PIN 23
#define Y_ENABLE_PIN 28
#define Y_MS1_PIN 69
#define Y_MS2_PIN 39

#define Z_STEP_PIN 35
#define Z_DIR_PIN 47
#define Z_MIN_PIN 10
#define Z_MAX_PIN 30
#define Z_ENABLE_PIN 27
#define Z_MS1_PIN 68
#define Z_MS2_PIN 67

#define HEATER_BED_PIN 3
#define TEMP_BED_PIN 2 

#define HEATER_0_PIN  9
#define TEMP_0_PIN 0

#define HEATER_1_PIN 7
#define TEMP_1_PIN 1

#define HEATER_2_PIN 6
#define TEMP_2_PIN -1

#define E0_STEP_PIN         34
#define E0_DIR_PIN          43
#define E0_ENABLE_PIN       26
#define E0_MS1_PIN 65
#define E0_MS2_PIN 66

#define E1_STEP_PIN         33
#define E1_DIR_PIN          42
#define E1_ENABLE_PIN       25
#define E1_MS1_PIN 63
#define E1_MS2_PIN 64

#define DIGIPOTSS_PIN 38
#define DIGIPOT_CHANNELS {4,5,3,0,1} // X Y Z E0 E1 digipot channels to stepper driver mapping

#define SDPOWER            -1
#define SDSS               53
#define LED_PIN            13
#define FAN_PIN            8
#define PS_ON_PIN          4
#define KILL_PIN           -1
#define SUICIDE_PIN        -1  //PIN that has to be turned on right after start, to keep power flowing.


/****************************************************************************************
* MegaTronics
*
****************************************************************************************/
#if MOTHERBOARD_EQ_70
#define KNOWN_BOARD 1

//////////////////FIX THIS//////////////

#if !defined(__AVR_ATmega2560__)
 #error Oops!  Make sure you have 'Arduino Mega' selected from the 'Tools -> Boards' menu.
#endif //!defined(__AVR_ATmega2560__)


#define LARGE_FLASH        true

#define X_STEP_PIN         26
#define X_DIR_PIN          28
#define X_ENABLE_PIN       24
#define X_MIN_PIN          41
#define X_MAX_PIN          37

#define Y_STEP_PIN         60 // A6
#define Y_DIR_PIN          61 // A7
#define Y_ENABLE_PIN       22
#define Y_MIN_PIN          14
#define Y_MAX_PIN          15

#define Z_STEP_PIN         54 // A0
#define Z_DIR_PIN          55 // A1
#define Z_ENABLE_PIN       56 // A2
#define Z_MIN_PIN          18
#define Z_MAX_PIN          19

#define E0_STEP_PIN        31
#define E0_DIR_PIN         32
#define E0_ENABLE_PIN      38

#define E1_STEP_PIN        34
#define E1_DIR_PIN         36
#define E1_ENABLE_PIN      30

#define SDPOWER            -1
#define SDSS               53
#define LED_PIN            13


#define FAN_PIN            7 // IO pin. Buffer needed
#define PS_ON_PIN          12
#define KILL_PIN           -1

#define HEATER_0_PIN       9    // EXTRUDER 1
#define HEATER_1_PIN       8    // EXTRUDER 2 (FAN On Sprinter)
#define HEATER_2_PIN       -1  

#if TEMP_SENSOR_0_EQ_MINUS_1
#define TEMP_0_PIN         8   // ANALOG NUMBERING
#else //TEMP_SENSOR_0_EQ_MINUS_1
#define TEMP_0_PIN         13   // ANALOG NUMBERING

#endif //TEMP_SENSOR_0_EQ_MINUS_1

#define TEMP_1_PIN         15   // ANALOG NUMBERING
#define TEMP_2_PIN         -1   // ANALOG NUMBERING
#define HEATER_BED_PIN     10   // BED
#define TEMP_BED_PIN       14   // ANALOG NUMBERING

#define BEEPER 33			// Beeper on AUX-4


#if defined(ULTRA_LCD)

#if defined(NEWPANEL)
  //arduino pin which triggers an piezzo beeper
    
    #define LCD_PINS_RS 16 
    #define LCD_PINS_ENABLE 17
    #define LCD_PINS_D4 23
    #define LCD_PINS_D5 25 
    #define LCD_PINS_D6 27
    #define LCD_PINS_D7 29
    
    //buttons are directly attached using AUX-2
    #define BTN_EN1 59
    #define BTN_EN2 64
    #define BTN_ENC 43  //the click
    
    #define BLEN_C 2
    #define BLEN_B 1
    #define BLEN_A 0
    
    #define SDCARDDETECT -1		// Ramps does not use this port
    
      //encoder rotation values
    #define encrot0 0
    #define encrot1 2
    #define encrot2 3
    #define encrot3 1
#endif //defined(NEWPANEL)
#endif //defined(ULTRA_LCD)

#endif //MOTHERBOARD_EQ_70

#if !defined(KNOWN_BOARD)
#error Unknown MOTHERBOARD value in configuration.h
#endif //!defined(KNOWN_BOARD)

//List of pins which to ignore when asked to change by gcode, 0 and 1 are RX and TX, do not mess with those!
#define _E0_PINS E0_STEP_PIN, E0_DIR_PIN, E0_ENABLE_PIN, HEATER_0_PIN, 
#if EXTRUDERS_GT_1
  #define _E1_PINS E1_STEP_PIN, E1_DIR_PIN, E1_ENABLE_PIN, HEATER_1_PIN,
#else //EXTRUDERS_GT_1
  #define _E1_PINS
#endif //EXTRUDERS_GT_1
#if EXTRUDERS_GT_2
  #define _E2_PINS E2_STEP_PIN, E2_DIR_PIN, E2_ENABLE_PIN, HEATER_2_PIN,
#else //EXTRUDERS_GT_2
  #define _E2_PINS
#endif //EXTRUDERS_GT_2

#if defined(X_STOP_PIN)
#if X_HOME_DIR_LT_ZERO
    #define X_MIN_PIN X_STOP_PIN
    #define X_MAX_PIN -1
#else //X_HOME_DIR_LT_ZERO
    #define X_MIN_PIN -1
    #define X_MAX_PIN X_STOP_PIN
#endif //X_HOME_DIR_LT_ZERO
#endif //defined(X_STOP_PIN)

#if defined(Y_STOP_PIN)
#if Y_HOME_DIR_LT_ZERO
    #define Y_MIN_PIN Y_STOP_PIN
    #define Y_MAX_PIN -1
#else //Y_HOME_DIR_LT_ZERO
    #define Y_MIN_PIN -1
    #define Y_MAX_PIN Y_STOP_PIN
#endif //Y_HOME_DIR_LT_ZERO
#endif //defined(Y_STOP_PIN)

#if defined(Z_STOP_PIN)
#if Z_HOME_DIR_LT_ZERO
    #define Z_MIN_PIN Z_STOP_PIN
    #define Z_MAX_PIN -1
#else //Z_HOME_DIR_LT_ZERO
    #define Z_MIN_PIN -1
    #define Z_MAX_PIN Z_STOP_PIN
#endif //Z_HOME_DIR_LT_ZERO
#endif //defined(Z_STOP_PIN)

#if defined(DISABLE_MAX_ENDSTOPS)
#define X_MAX_PIN          -1
#define Y_MAX_PIN          -1
#define Z_MAX_PIN          -1
#endif //defined(DISABLE_MAX_ENDSTOPS)

#define SENSITIVE_PINS {0, 1, X_STEP_PIN, X_DIR_PIN, X_ENABLE_PIN, X_MIN_PIN, X_MAX_PIN, Y_STEP_PIN, Y_DIR_PIN, Y_ENABLE_PIN, Y_MIN_PIN, Y_MAX_PIN, Z_STEP_PIN, Z_DIR_PIN, Z_ENABLE_PIN, Z_MIN_PIN, Z_MAX_PIN, PS_ON_PIN, \
                        HEATER_BED_PIN, FAN_PIN,                  \
                        _E0_PINS _E1_PINS _E2_PINS             \
                        analogInputToDigitalPin(TEMP_0_PIN), analogInputToDigitalPin(TEMP_1_PIN), analogInputToDigitalPin(TEMP_2_PIN), analogInputToDigitalPin(TEMP_BED_PIN) }
