/**
 * English
 *
 * LCD Menu Messages
 * Se also documentation/LCDLanguageFont.md
 *
 */
#define LANGUAGE_EN_H

#if !(defined(MAPPER_NON) || (defined(MAPPER_C2C3) || (defined(MAPPER_D0D1) || (defined(MAPPER_D0D1_MOD) || defined(MAPPER_E382E383)))))
  #define MAPPER_NON         // For direct asci codes
#endif //!(defined(MAPPER_NON) || (defined(MAPPER_C2C3) || (defined(MAPPER_D0D1) || (defined(MAPPER_D0D1_MOD) || defined(MAPPER_E382E383)))))

//#define SIMULATE_ROMFONT //Comment in to see what is seen on the character based displays
#if !(defined(SIMULATE_ROMFONT) || (defined(DISPLAY_CHARSET_ISO10646_1) || (defined(DISPLAY_CHARSET_ISO10646_5) || (defined(DISPLAY_CHARSET_ISO10646_KANA) || defined(DISPLAY_CHARSET_ISO10646_CN)))))
  #define DISPLAY_CHARSET_ISO10646_1 // use the better font on full graphic displays.
#endif //!(defined(SIMULATE_ROMFONT) || (defined(DISPLAY_CHARSET_ISO10646_1) || (defined(DISPLAY_CHARSET_ISO10646_5) || (defined(DISPLAY_CHARSET_ISO10646_KANA) || defined(DISPLAY_CHARSET_ISO10646_CN)))))


#if !defined(WELCOME_MSG)
#define WELCOME_MSG                         MACHINE_NAME " ready."
#endif //!defined(WELCOME_MSG)
#if !defined(MSG_SD_INSERTED)
#define MSG_SD_INSERTED                     "Card inserted"
#endif //!defined(MSG_SD_INSERTED)
#if !defined(MSG_SD_REMOVED)
#define MSG_SD_REMOVED                      "Card removed"
#endif //!defined(MSG_SD_REMOVED)
#if !defined(MSG_MAIN)
#define MSG_MAIN                            "Main"
#endif //!defined(MSG_MAIN)
#if !defined(MSG_AUTOSTART)
#define MSG_AUTOSTART                       "Autostart"
#endif //!defined(MSG_AUTOSTART)
#if !defined(MSG_DISABLE_STEPPERS)
#define MSG_DISABLE_STEPPERS                "Disable steppers"
#endif //!defined(MSG_DISABLE_STEPPERS)
#if !defined(MSG_AUTO_HOME)
#define MSG_AUTO_HOME                       "Auto home"
#endif //!defined(MSG_AUTO_HOME)
#if !defined(MSG_SET_HOME_OFFSETS)
#define MSG_SET_HOME_OFFSETS                "Set home offsets"
#endif //!defined(MSG_SET_HOME_OFFSETS)
#if !defined(MSG_SET_ORIGIN)
#define MSG_SET_ORIGIN                      "Set origin"
#endif //!defined(MSG_SET_ORIGIN)
#if !defined(MSG_PREHEAT_PLA)
#define MSG_PREHEAT_PLA                     "Preheat PLA"
#endif //!defined(MSG_PREHEAT_PLA)
#if !defined(MSG_PREHEAT_PLA_N)
#define MSG_PREHEAT_PLA_N                   MSG_PREHEAT_PLA " "
#endif //!defined(MSG_PREHEAT_PLA_N)
#if !defined(MSG_PREHEAT_PLA_ALL)
#define MSG_PREHEAT_PLA_ALL                 MSG_PREHEAT_PLA " All"
#endif //!defined(MSG_PREHEAT_PLA_ALL)
#if !defined(MSG_PREHEAT_PLA_BEDONLY)
#define MSG_PREHEAT_PLA_BEDONLY             MSG_PREHEAT_PLA " Bed"
#endif //!defined(MSG_PREHEAT_PLA_BEDONLY)
#if !defined(MSG_PREHEAT_PLA_SETTINGS)
#define MSG_PREHEAT_PLA_SETTINGS            MSG_PREHEAT_PLA " conf"
#endif //!defined(MSG_PREHEAT_PLA_SETTINGS)
#if !defined(MSG_PREHEAT_ABS)
#define MSG_PREHEAT_ABS                     "Preheat ABS"
#endif //!defined(MSG_PREHEAT_ABS)
#if !defined(MSG_PREHEAT_ABS_N)
#define MSG_PREHEAT_ABS_N                   MSG_PREHEAT_ABS " "
#endif //!defined(MSG_PREHEAT_ABS_N)
#if !defined(MSG_PREHEAT_ABS_ALL)
#define MSG_PREHEAT_ABS_ALL                 MSG_PREHEAT_ABS " All"
#endif //!defined(MSG_PREHEAT_ABS_ALL)
#if !defined(MSG_PREHEAT_ABS_BEDONLY)
#define MSG_PREHEAT_ABS_BEDONLY             MSG_PREHEAT_ABS " Bed"
#endif //!defined(MSG_PREHEAT_ABS_BEDONLY)
#if !defined(MSG_PREHEAT_ABS_SETTINGS)
#define MSG_PREHEAT_ABS_SETTINGS            MSG_PREHEAT_ABS " conf"
#endif //!defined(MSG_PREHEAT_ABS_SETTINGS)
#if !defined(MSG_H1)
#define MSG_H1                              "1"
#endif //!defined(MSG_H1)
#if !defined(MSG_H2)
#define MSG_H2                              "2"
#endif //!defined(MSG_H2)
#if !defined(MSG_H3)
#define MSG_H3                              "3"
#endif //!defined(MSG_H3)
#if !defined(MSG_H4)
#define MSG_H4                              "4"
#endif //!defined(MSG_H4)
#if !defined(MSG_COOLDOWN)
#define MSG_COOLDOWN                        "Cooldown"
#endif //!defined(MSG_COOLDOWN)
#if !defined(MSG_SWITCH_PS_ON)
#define MSG_SWITCH_PS_ON                    "Switch power on"
#endif //!defined(MSG_SWITCH_PS_ON)
#if !defined(MSG_SWITCH_PS_OFF)
#define MSG_SWITCH_PS_OFF                   "Switch power off"
#endif //!defined(MSG_SWITCH_PS_OFF)
#if !defined(MSG_EXTRUDE)
#define MSG_EXTRUDE                         "Extrude"
#endif //!defined(MSG_EXTRUDE)
#if !defined(MSG_RETRACT)
#define MSG_RETRACT                         "Retract"
#endif //!defined(MSG_RETRACT)
#if !defined(MSG_MOVE_AXIS)
#define MSG_MOVE_AXIS                       "Move axis"
#endif //!defined(MSG_MOVE_AXIS)
#if !defined(MSG_LEVEL_BED)
#define MSG_LEVEL_BED                       "Level bed"
#endif //!defined(MSG_LEVEL_BED)
#if !defined(MSG_MOVE_X)
#define MSG_MOVE_X                          "Move X"
#endif //!defined(MSG_MOVE_X)
#if !defined(MSG_MOVE_Y)
#define MSG_MOVE_Y                          "Move Y"
#endif //!defined(MSG_MOVE_Y)
#if !defined(MSG_MOVE_Z)
#define MSG_MOVE_Z                          "Move Z"
#endif //!defined(MSG_MOVE_Z)
#if !defined(MSG_MOVE_E)
#define MSG_MOVE_E                          "Extruder"
#endif //!defined(MSG_MOVE_E)
#if !defined(MSG_MOVE_01MM)
#define MSG_MOVE_01MM                       "Move 0.1mm"
#endif //!defined(MSG_MOVE_01MM)
#if !defined(MSG_MOVE_1MM)
#define MSG_MOVE_1MM                        "Move 1mm"
#endif //!defined(MSG_MOVE_1MM)
#if !defined(MSG_MOVE_10MM)
#define MSG_MOVE_10MM                       "Move 10mm"
#endif //!defined(MSG_MOVE_10MM)
#if !defined(MSG_SPEED)
#define MSG_SPEED                           "Speed"
#endif //!defined(MSG_SPEED)
#if !defined(MSG_NOZZLE)
#define MSG_NOZZLE                          "Nozzle"
#endif //!defined(MSG_NOZZLE)
#if !defined(MSG_N0)
#define MSG_N0                              " 0"
#endif //!defined(MSG_N0)
#if !defined(MSG_N1)
#define MSG_N1                              " 1"
#endif //!defined(MSG_N1)
#if !defined(MSG_N2)
#define MSG_N2                              " 2"
#endif //!defined(MSG_N2)
#if !defined(MSG_N3)
#define MSG_N3                              " 3"
#endif //!defined(MSG_N3)
#if !defined(MSG_N4)
#define MSG_N4                              " 4"
#endif //!defined(MSG_N4)
#if !defined(MSG_BED)
#define MSG_BED                             "Bed"
#endif //!defined(MSG_BED)
#if !defined(MSG_FAN_SPEED)
#define MSG_FAN_SPEED                       "Fan speed"
#endif //!defined(MSG_FAN_SPEED)
#if !defined(MSG_FLOW)
#define MSG_FLOW                            "Flow"
#endif //!defined(MSG_FLOW)
#if !defined(MSG_CONTROL)
#define MSG_CONTROL                         "Control"
#endif //!defined(MSG_CONTROL)
#if !defined(MSG_MIN)
#define MSG_MIN                             " "LCD_STR_THERMOMETER " Min"
#endif //!defined(MSG_MIN)
#if !defined(MSG_MAX)
#define MSG_MAX                             " "LCD_STR_THERMOMETER " Max"
#endif //!defined(MSG_MAX)
#if !defined(MSG_FACTOR)
#define MSG_FACTOR                          " "LCD_STR_THERMOMETER " Fact"
#endif //!defined(MSG_FACTOR)
#if !defined(MSG_AUTOTEMP)
#define MSG_AUTOTEMP                        "Autotemp"
#endif //!defined(MSG_AUTOTEMP)
#if !defined(MSG_ON)
#define MSG_ON                              "On "
#endif //!defined(MSG_ON)
#if !defined(MSG_OFF)
#define MSG_OFF                             "Off"
#endif //!defined(MSG_OFF)
#if !defined(MSG_PID_P)
#define MSG_PID_P                           "PID-P"
#endif //!defined(MSG_PID_P)
#if !defined(MSG_PID_I)
#define MSG_PID_I                           "PID-I"
#endif //!defined(MSG_PID_I)
#if !defined(MSG_PID_D)
#define MSG_PID_D                           "PID-D"
#endif //!defined(MSG_PID_D)
#if !defined(MSG_PID_C)
#define MSG_PID_C                           "PID-C"
#endif //!defined(MSG_PID_C)
#if !defined(MSG_E2)
#define MSG_E2                              " E2"
#endif //!defined(MSG_E2)
#if !defined(MSG_E3)
#define MSG_E3                              " E3"
#endif //!defined(MSG_E3)
#if !defined(MSG_E4)
#define MSG_E4                              " E4"
#endif //!defined(MSG_E4)
#if !defined(MSG_ACC)
#define MSG_ACC                             "Accel"
#endif //!defined(MSG_ACC)
#if !defined(MSG_VXY_JERK)
#define MSG_VXY_JERK                        "Vxy-jerk"
#endif //!defined(MSG_VXY_JERK)
#if !defined(MSG_VZ_JERK)
#define MSG_VZ_JERK                         "Vz-jerk"
#endif //!defined(MSG_VZ_JERK)
#if !defined(MSG_VE_JERK)
#define MSG_VE_JERK                         "Ve-jerk"
#endif //!defined(MSG_VE_JERK)
#if !defined(MSG_VMAX)
#define MSG_VMAX                            "Vmax "
#endif //!defined(MSG_VMAX)
#if !defined(MSG_X)
#define MSG_X                               "x"
#endif //!defined(MSG_X)
#if !defined(MSG_Y)
#define MSG_Y                               "y"
#endif //!defined(MSG_Y)
#if !defined(MSG_Z)
#define MSG_Z                               "z"
#endif //!defined(MSG_Z)
#if !defined(MSG_E)
#define MSG_E                               "e"
#endif //!defined(MSG_E)
#if !defined(MSG_VMIN)
#define MSG_VMIN                            "Vmin"
#endif //!defined(MSG_VMIN)
#if !defined(MSG_VTRAV_MIN)
#define MSG_VTRAV_MIN                       "VTrav min"
#endif //!defined(MSG_VTRAV_MIN)
#if !defined(MSG_AMAX)
#define MSG_AMAX                            "Amax "
#endif //!defined(MSG_AMAX)
#if !defined(MSG_A_RETRACT)
#define MSG_A_RETRACT                       "A-retract"
#endif //!defined(MSG_A_RETRACT)
#if !defined(MSG_A_TRAVEL)
#define MSG_A_TRAVEL                        "A-travel"
#endif //!defined(MSG_A_TRAVEL)
#if !defined(MSG_XSTEPS)
#define MSG_XSTEPS                          "Xsteps/mm"
#endif //!defined(MSG_XSTEPS)
#if !defined(MSG_YSTEPS)
#define MSG_YSTEPS                          "Ysteps/mm"
#endif //!defined(MSG_YSTEPS)
#if !defined(MSG_ZSTEPS)
#define MSG_ZSTEPS                          "Zsteps/mm"
#endif //!defined(MSG_ZSTEPS)
#if !defined(MSG_ESTEPS)
#define MSG_ESTEPS                          "Esteps/mm"
#endif //!defined(MSG_ESTEPS)
#if !defined(MSG_TEMPERATURE)
#define MSG_TEMPERATURE                     "Temperature"
#endif //!defined(MSG_TEMPERATURE)
#if !defined(MSG_MOTION)
#define MSG_MOTION                          "Motion"
#endif //!defined(MSG_MOTION)
#if !defined(MSG_VOLUMETRIC)
#define MSG_VOLUMETRIC                      "Filament"
#endif //!defined(MSG_VOLUMETRIC)
#if !defined(MSG_VOLUMETRIC_ENABLED)
#define MSG_VOLUMETRIC_ENABLED              "E in mm3"
#endif //!defined(MSG_VOLUMETRIC_ENABLED)
#if !defined(MSG_FILAMENT_SIZE_EXTRUDER_0)
#define MSG_FILAMENT_SIZE_EXTRUDER_0        "Fil. Dia. 1"
#endif //!defined(MSG_FILAMENT_SIZE_EXTRUDER_0)
#if !defined(MSG_FILAMENT_SIZE_EXTRUDER_1)
#define MSG_FILAMENT_SIZE_EXTRUDER_1        "Fil. Dia. 2"
#endif //!defined(MSG_FILAMENT_SIZE_EXTRUDER_1)
#if !defined(MSG_FILAMENT_SIZE_EXTRUDER_2)
#define MSG_FILAMENT_SIZE_EXTRUDER_2        "Fil. Dia. 3"
#endif //!defined(MSG_FILAMENT_SIZE_EXTRUDER_2)
#if !defined(MSG_FILAMENT_SIZE_EXTRUDER_3)
#define MSG_FILAMENT_SIZE_EXTRUDER_3        "Fil. Dia. 4"
#endif //!defined(MSG_FILAMENT_SIZE_EXTRUDER_3)
#if !defined(MSG_CONTRAST)
#define MSG_CONTRAST                        "LCD contrast"
#endif //!defined(MSG_CONTRAST)
#if !defined(MSG_STORE_EPROM)
#define MSG_STORE_EPROM                     "Store memory"
#endif //!defined(MSG_STORE_EPROM)
#if !defined(MSG_LOAD_EPROM)
#define MSG_LOAD_EPROM                      "Load memory"
#endif //!defined(MSG_LOAD_EPROM)
#if !defined(MSG_RESTORE_FAILSAFE)
#define MSG_RESTORE_FAILSAFE                "Restore failsafe"
#endif //!defined(MSG_RESTORE_FAILSAFE)
#if !defined(MSG_REFRESH)
#define MSG_REFRESH                         "Refresh"
#endif //!defined(MSG_REFRESH)
#if !defined(MSG_WATCH)
#define MSG_WATCH                           "Info screen"
#endif //!defined(MSG_WATCH)
#if !defined(MSG_PREPARE)
#define MSG_PREPARE                         "Prepare"
#endif //!defined(MSG_PREPARE)
#if !defined(MSG_TUNE)
#define MSG_TUNE                            "Tune"
#endif //!defined(MSG_TUNE)
#if !defined(MSG_PAUSE_PRINT)
#define MSG_PAUSE_PRINT                     "Pause print"
#endif //!defined(MSG_PAUSE_PRINT)
#if !defined(MSG_RESUME_PRINT)
#define MSG_RESUME_PRINT                    "Resume print"
#endif //!defined(MSG_RESUME_PRINT)
#if !defined(MSG_STOP_PRINT)
#define MSG_STOP_PRINT                      "Stop print"
#endif //!defined(MSG_STOP_PRINT)
#if !defined(MSG_CARD_MENU)
#define MSG_CARD_MENU                       "Print from SD"
#endif //!defined(MSG_CARD_MENU)
#if !defined(MSG_NO_CARD)
#define MSG_NO_CARD                         "No SD card"
#endif //!defined(MSG_NO_CARD)
#if !defined(MSG_DWELL)
#define MSG_DWELL                           "Sleep..."
#endif //!defined(MSG_DWELL)
#if !defined(MSG_USERWAIT)
#define MSG_USERWAIT                        "Wait for user..."
#endif //!defined(MSG_USERWAIT)
#if !defined(MSG_RESUMING)
#define MSG_RESUMING                        "Resuming print"
#endif //!defined(MSG_RESUMING)
#if !defined(MSG_PRINT_ABORTED)
#define MSG_PRINT_ABORTED                   "Print aborted"
#endif //!defined(MSG_PRINT_ABORTED)
#if !defined(MSG_NO_MOVE)
#define MSG_NO_MOVE                         "No move."
#endif //!defined(MSG_NO_MOVE)
#if !defined(MSG_KILLED)
#define MSG_KILLED                          "KILLED. "
#endif //!defined(MSG_KILLED)
#if !defined(MSG_STOPPED)
#define MSG_STOPPED                         "STOPPED. "
#endif //!defined(MSG_STOPPED)
#if !defined(MSG_CONTROL_RETRACT)
#define MSG_CONTROL_RETRACT                 "Retract mm"
#endif //!defined(MSG_CONTROL_RETRACT)
#if !defined(MSG_CONTROL_RETRACT_SWAP)
#define MSG_CONTROL_RETRACT_SWAP            "Swap Re.mm"
#endif //!defined(MSG_CONTROL_RETRACT_SWAP)
#if !defined(MSG_CONTROL_RETRACTF)
#define MSG_CONTROL_RETRACTF                "Retract  V"
#endif //!defined(MSG_CONTROL_RETRACTF)
#if !defined(MSG_CONTROL_RETRACT_ZLIFT)
#define MSG_CONTROL_RETRACT_ZLIFT           "Hop mm"
#endif //!defined(MSG_CONTROL_RETRACT_ZLIFT)
#if !defined(MSG_CONTROL_RETRACT_RECOVER)
#define MSG_CONTROL_RETRACT_RECOVER         "UnRet +mm"
#endif //!defined(MSG_CONTROL_RETRACT_RECOVER)
#if !defined(MSG_CONTROL_RETRACT_RECOVER_SWAP)
#define MSG_CONTROL_RETRACT_RECOVER_SWAP    "S UnRet+mm"
#endif //!defined(MSG_CONTROL_RETRACT_RECOVER_SWAP)
#if !defined(MSG_CONTROL_RETRACT_RECOVERF)
#define MSG_CONTROL_RETRACT_RECOVERF        "UnRet  V"
#endif //!defined(MSG_CONTROL_RETRACT_RECOVERF)
#if !defined(MSG_AUTORETRACT)
#define MSG_AUTORETRACT                     "AutoRetr."
#endif //!defined(MSG_AUTORETRACT)
#if !defined(MSG_FILAMENTCHANGE)
#define MSG_FILAMENTCHANGE                  "Change filament"
#endif //!defined(MSG_FILAMENTCHANGE)
#if !defined(MSG_INIT_SDCARD)
#define MSG_INIT_SDCARD                     "Init. SD card"
#endif //!defined(MSG_INIT_SDCARD)
#if !defined(MSG_CNG_SDCARD)
#define MSG_CNG_SDCARD                      "Change SD card"
#endif //!defined(MSG_CNG_SDCARD)
#if !defined(MSG_ZPROBE_OUT)
#define MSG_ZPROBE_OUT                      "Z probe out. bed"
#endif //!defined(MSG_ZPROBE_OUT)
#if !defined(MSG_POSITION_UNKNOWN)
#define MSG_POSITION_UNKNOWN                "Home X/Y before Z"
#endif //!defined(MSG_POSITION_UNKNOWN)
#if !defined(MSG_ZPROBE_ZOFFSET)
#define MSG_ZPROBE_ZOFFSET                  "Z Offset"
#endif //!defined(MSG_ZPROBE_ZOFFSET)
#if !defined(MSG_BABYSTEP_X)
#define MSG_BABYSTEP_X                      "Babystep X"
#endif //!defined(MSG_BABYSTEP_X)
#if !defined(MSG_BABYSTEP_Y)
#define MSG_BABYSTEP_Y                      "Babystep Y"
#endif //!defined(MSG_BABYSTEP_Y)
#if !defined(MSG_BABYSTEP_Z)
#define MSG_BABYSTEP_Z                      "Babystep Z"
#endif //!defined(MSG_BABYSTEP_Z)
#if !defined(MSG_ENDSTOP_ABORT)
#define MSG_ENDSTOP_ABORT                   "Endstop abort"
#endif //!defined(MSG_ENDSTOP_ABORT)
#if !defined(MSG_HEATING_FAILED_LCD)
#define MSG_HEATING_FAILED_LCD              "Heating failed"
#endif //!defined(MSG_HEATING_FAILED_LCD)
#if !defined(MSG_ERR_REDUNDANT_TEMP)
#define MSG_ERR_REDUNDANT_TEMP              "Err: REDUNDANT TEMP ERROR"
#endif //!defined(MSG_ERR_REDUNDANT_TEMP)
#if !defined(MSG_THERMAL_RUNAWAY)
#define MSG_THERMAL_RUNAWAY                 "THERMAL RUNAWAY"
#endif //!defined(MSG_THERMAL_RUNAWAY)
#if !defined(MSG_ERR_MAXTEMP)
#define MSG_ERR_MAXTEMP                     "Err: MAXTEMP"
#endif //!defined(MSG_ERR_MAXTEMP)
#if !defined(MSG_ERR_MINTEMP)
#define MSG_ERR_MINTEMP                     "Err: MINTEMP"
#endif //!defined(MSG_ERR_MINTEMP)
#if !defined(MSG_ERR_MAXTEMP_BED)
#define MSG_ERR_MAXTEMP_BED                 "Err: MAXTEMP BED"
#endif //!defined(MSG_ERR_MAXTEMP_BED)
#if !defined(MSG_ERR_MINTEMP_BED)
#define MSG_ERR_MINTEMP_BED                 "Err: MINTEMP BED"
#endif //!defined(MSG_ERR_MINTEMP_BED)
#if !defined(MSG_END_HOUR)
#define MSG_END_HOUR                        "hours"
#endif //!defined(MSG_END_HOUR)
#if !defined(MSG_END_MINUTE)
#define MSG_END_MINUTE                      "minutes"
#endif //!defined(MSG_END_MINUTE)
+#define MSG_HEATING                         "Heating..."
+#define MSG_HEATING_COMPLETE                "Heating done."
+#define MSG_BED_HEATING                     "Bed Heating."
+#define MSG_BED_DONE                        "Bed done."

#if defined(DELTA_CALIBRATION_MENU)
#if !defined(MSG_DELTA_CALIBRATE)
  #define MSG_DELTA_CALIBRATE             "Delta Calibration"
#endif //!defined(MSG_DELTA_CALIBRATE)
#if !defined(MSG_DELTA_CALIBRATE_X)
  #define MSG_DELTA_CALIBRATE_X           "Calibrate X"
#endif //!defined(MSG_DELTA_CALIBRATE_X)
#if !defined(MSG_DELTA_CALIBRATE_Y)
  #define MSG_DELTA_CALIBRATE_Y           "Calibrate Y"
#endif //!defined(MSG_DELTA_CALIBRATE_Y)
#if !defined(MSG_DELTA_CALIBRATE_Z)
  #define MSG_DELTA_CALIBRATE_Z           "Calibrate Z"
#endif //!defined(MSG_DELTA_CALIBRATE_Z)
#if !defined(MSG_DELTA_CALIBRATE_CENTER)
  #define MSG_DELTA_CALIBRATE_CENTER      "Calibrate Center"
#endif //!defined(MSG_DELTA_CALIBRATE_CENTER)
#endif //defined(DELTA_CALIBRATION_MENU)

